#!/bin/bash
# $1 - project name
# $2 - apps folder
./docker-build.sh
docker run --name ionic -p 8080:80 -v $2:/root/apps/ -di ionic
docker exec -i ionic /bin/bash /root/buildscript.sh $1
docker stop ionic
docker rm ionic
