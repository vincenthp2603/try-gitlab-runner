#!/bin/bash
cd /root/apps/
#chown -R root:root /root/apps
yes | npm install && \
#npm rebuild node-sass && \
#export NODE_OPTIONS="--max-old-space-size=4000" 
echo Y | cordova platform prepare android
ionic cordova platform add ios
ionic cordova prepare ios 
#echo y | android update sdk -u -a -t 41,8,53,55,2 && \
yes | $ANDROID_SDK_ROOT/tools/bin/sdkmanager --licenses
ionic cordova build android --prod --no-interactive --release
cd platforms/android/app/build/outputs/apk/release
echo -e "cross4channel\ncross4channel\nCross4Channel GmbH\nDevelopment\nCross4Channel\nBerlin\nBerlin\nDE\nyes" | keytool -genkey -v -keystore release-key.keystore -alias cross4channel -keyalg RSA -keysize 2048 -validity 10000
echo "cross4channel" | jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore release-key.keystore app-release-unsigned.apk cross4channel
mv app-release-unsigned.apk $1.apk
cp $1.apk /root/apps

cp -r /root/apps/platforms /root/apps/$1-platforms
chmod -R 777 /root/apps



